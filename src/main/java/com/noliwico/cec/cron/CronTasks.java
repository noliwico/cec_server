package com.noliwico.cec.cron;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.noliwico.cec.logic.GameLogic;

@Lazy(false)
@Component
public class CronTasks {
//	CRON: <second> <minute> <hour> <day-of-month> <month> <day-of-week> <year>
// <year> is optional	
	
	
//	@Scheduled(cron = "0 * * * * *")
//  @Scheduled(fixedDelay = 60000, initialDelay = 30000)	
	
	private GameLogic gameLogic;
	
	@Autowired
	public void setGameLogic( GameLogic gameLogic ) {
		this.gameLogic = gameLogic;
	}
	
	@Scheduled(fixedDelay = 60000, initialDelay = 60000)	
	public void removeTimeOutGames() {
		gameLogic.removeTimeOutGames();
	}
}
