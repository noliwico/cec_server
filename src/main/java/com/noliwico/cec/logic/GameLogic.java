package com.noliwico.cec.logic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Random;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Qualifier("gameLogic")
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class GameLogic {
	private ArrayList<Game> games;
	private HashMap<String, ArrayList<Game>> gameHistory;
	private Random random;

	public GameLogic() {
		this.games = new ArrayList<Game>();
		this.gameHistory = new HashMap<String, ArrayList<Game>>();
		this.random = new Random(System.currentTimeMillis());
	}
	
	public Game createNewGame(String name, int maxPlayers) {
		Game game = new Game();
		game.setAdminId(Long.toString(UUID.randomUUID().getMostSignificantBits()));
		game.setId(Long.toString(UUID.randomUUID().getMostSignificantBits()));
		game.setMaxPlayers(maxPlayers>GameConstants.MAX_PLAYERS?GameConstants.MAX_PLAYERS:(maxPlayers<GameConstants.MIN_PLAYERS)?GameConstants.MIN_PLAYERS:maxPlayers);
		game.setName(name);
		this.games.add(game);
		return game;
	}

	
	public synchronized boolean closeGame(String gameId, String adminId) {
		boolean success = false;
		for (Iterator<Game> iterator = games.iterator(); iterator.hasNext();) {
			Game game = iterator.next();
			if ( game.getId().equals(gameId)) {
				if (  game.getAdminId().equals(adminId) ) {
					games.remove(game);
					success = true;
					break;
				}
				break;
			}
			
		}
		return success;
	}
		
	public Game getGameById(String id) {
		Game retVal = null;
		for (Iterator<Game> iterator = games.iterator(); iterator.hasNext();) {
			Game game = iterator.next();
			if ( game.getId().equals(id)) {
				retVal = game;
				break;
			}
		}
		return retVal;
	}
	
	public synchronized Player signUp(String gameId, String playerName, ErrorHolder errors) {
		Game game = getGameById(gameId);
		if ( game == null ) {
			errors.setErrorCode(GameConstants.ERROR_CODE_NO_GAME_BY_GAMEID);
			errors.setErrorMessage(GameConstants.ERROR_MESSAGEE_NO_GAME_BY_GAMEID);
			return null;
		}
		if ( game.getMaxPlayers() <= game.getPlayers().size() ) {
			errors.setErrorCode(GameConstants.ERROR_CODE_GAME_IS_FULL);
			errors.setErrorMessage(GameConstants.ERROR_MESSAGE_GAME_IS_FULL);
			return null;
		}		
		
		if ( game.getGameStatus() != GameConstants.STATUS_CREATED ) {
			errors.setErrorCode(GameConstants.ERROR_CODE_GAME_IS_FULL);
			errors.setErrorMessage(GameConstants.ERROR_MESSAGE_GAME_IS_FULL);
			return null;
		}
		Player player = new Player();
		player.setHp(GameConstants.DEFAULT_PLAYER_HP);
		player.setId(Long.toString(UUID.randomUUID().getMostSignificantBits()));
		player.setName(playerName);
		player.setNpc(false);
		player.setSecret(Long.toString(UUID.randomUUID().getMostSignificantBits()));
		game.getPlayers().add(player);
		
		if ( game.getPlayers().size() == game.getMaxPlayers() ) {
			this.startGame(game);
		}
		return player;
	}
	
	private void generateNonPlayerCubes( Game game ) {
		while ( game.getCubes().size() < game.getBoardHeight() / GameConstants.EXPECTED_CUBE_COUNT ) {
			int x = random.nextInt(game.getBoardWidth());
			int y = random.nextInt(game.getBoardHeight());
			boolean match = false;
			for ( Iterator<Player> players = game.getPlayers().iterator(); players.hasNext(); ) {
				Player player = players.next();
				if ( x == player.getPosX() && y == player.getPosY() && player.getHp() > 0) {
					match = true;
					break;
				}
			}
			for ( Iterator<Cube> cubes = game.getCubes().iterator(); cubes.hasNext() && !match; ) {
				Cube cube = cubes.next();
				if ( x == cube.getPosX() && y == cube.getPosY() ) {
					match = true;
					break;
				}
			}
			
			if ( !match ) {
				Cube cube = new Cube();
				cube.setHp(GameConstants.DEFAULT_CUBE_HP);
				cube.setId(Long.toString(UUID.randomUUID().getMostSignificantBits()));
				cube.setNpc(true);
				cube.setPosX(x);
				cube.setPosY(y);
				game.getCubes().add(cube);
			}
			
		}
	}
	
	private void startGame( Game game) {
		ArrayList<Position> startPositions = new ArrayList<Position>();
		startPositions.add(new Position(0,0));
		startPositions.add(new Position(0,game.getBoardHeight()-1));
		startPositions.add(new Position(game.getBoardWidth() -1 ,game.getBoardHeight()-1));
		startPositions.add(new Position(game.getBoardWidth() -1 ,0));
		int i = 0;
		Collections.shuffle(game.getPlayers());
		
		for ( Iterator<Player> players = game.getPlayers().iterator(); players.hasNext();) {
			Player player = players.next();
			player.setPosX(startPositions.get(i).getX());
			player.setPosY(startPositions.get(i++).getY());
		}
		this.generateNonPlayerCubes( game );
		game.setGameStatus(GameConstants.STATUS_RUNNING);
		this.gameHistory.put(game.getId(), new ArrayList<Game>());
		this.gameHistory.get(game.getId()).add(new Game(game));
	
	}
	
	public boolean doStep(Game game, Player player, int dX, int dY) {

		int newX = player.getPosX() + dX;
		int newY = player.getPosY() + dY;
		if ( newX < 0 || newX > game.getBoardWidth() - 1) {
			player.setHp(player.getHp()-GameConstants.DECREASE_HP_BY_OUT_BORDER);

		} else	if ( newY < 0 || newY > game.getBoardHeight() - 1) {
			player.setHp(player.getHp()-GameConstants.DECREASE_HP_BY_OUT_BORDER);
		}
		player.setHp(player.getHp()-GameConstants.DECREASE_HP_BY_STEP);
		if (player.getHp() <= 0 ) {
			game.getPlayers().remove(player);
			return true;
		}
			
		Player targetPlayer = null;
		for ( Iterator<Player> players = game.getPlayers().iterator(); players.hasNext(); ) {
			Player currentPlayer = players.next();
			if ( currentPlayer.getPosX() == newX && currentPlayer.getPosY() == newY && currentPlayer.getHp() > 0) {
				targetPlayer = currentPlayer;
				break;
			}
		}
		
		if ( targetPlayer != null ) {
			//FIGHT!!!
			boolean targetKilled = false;
			int attackValue = 0;
			if ( targetPlayer.getHp() < player.getHp() * GameConstants.FIGHT_MIN_HP ) {
				targetKilled = true;

			} else {
				attackValue = (int)Math.round(( player.getHp() * ( 1 - GameConstants.FIGHT_CALC_LOW ) * random.nextDouble() ) + (player.getHp() * GameConstants.FIGHT_CALC_LOW));
				if ( attackValue >= targetPlayer.getHp() ) {
					targetKilled = true;
				}
			}
			if ( targetKilled ) {
				player.setHp(player.getHp() + targetPlayer.getHp());
				targetPlayer.setHp(0);
				targetPlayer.setPosX(-1);
				targetPlayer.setPosY(-1);
				game.getPlayers().remove(targetPlayer);
				player.setPosX(newX);
				player.setPosY(newY);
				return true;				
			} else {
				player.setHp(player.getHp() + attackValue);
				targetPlayer.setHp(targetPlayer.getHp() - attackValue);
			}
			return true;
		}
		
		Cube targetCube = null;
		for ( Iterator<Cube> cubes = game.getCubes().iterator(); cubes.hasNext(); ) {
			Cube currentCube = cubes.next();
			if ( currentCube.getPosX() == newX && currentCube.getPosY() == newY ) {
				targetCube = currentCube;
				break;
			}
		}		
		if ( targetCube != null ) {
			player.setHp(player.getHp() + targetCube.getHp());
			game.getCubes().remove(targetCube);
		}
		player.setPosX(newX);
		player.setPosY(newY);
		this.generateNonPlayerCubes(game);
		return true;
	}
	
	public ArrayList<Game> getGames() {
		return games;
	}

	public void setGames(ArrayList<Game> games) {
		this.games = games;
	}

	public void nextPlayer(Game game) {
		if ( game.getPlayers().size() < 2 ) {
			game.setCpi(0);
			game.setGameStatus(GameConstants.STATUS_FINISHED);
		}
		this.gameHistory.get(game.getId()).add(new Game(game));
		if (game.getGameStatus() == GameConstants.STATUS_FINISHED) {
			return;
		}
		if ( game.getCpi() < game.getPlayers().size() - 1) {
			game.setCpi(game.getCpi() + 1);
		} else {
			game.setCpi(0);
			game.setTurnCounter(game.getTurnCounter() + 1);
			if (game.getTurnCounter()%GameConstants.DECREASE_BOARD_BY_TURN == 0) {
				game.setBoardHeight( game.getBoardHeight() -1 );
				game.setBoardWidth(game.getBoardWidth() -1 );
			}
		}
	}

	public void removeTimeOutGames() {
		long currTime = System.currentTimeMillis();
		for (int i = 0; i < games.size();) {
			Game game =  games.get(i);

			if ( game.getCreated() + GameConstants.GAME_TIMEOUT < currTime ) {
				System.out.println("game removed: " + game.getName());

				games.remove(game);
			} else {
				i++;
			}
		}
		
	}

	public HashMap<String, ArrayList<Game>> getGameHistory() {
		return gameHistory;
	}

	public void setGameHistory(HashMap<String, ArrayList<Game>> gameHistory) {
		this.gameHistory = gameHistory;
	}
	

}
