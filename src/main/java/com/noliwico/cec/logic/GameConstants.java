package com.noliwico.cec.logic;

public class GameConstants {
	static public int STATUS_CREATED = 0;
	static public int STATUS_RUNNING = 1;
	static public int STATUS_FINISHED = 2;
	static public int MIN_PLAYERS = 2;
	static public int MAX_PLAYERS = 4;
	
	static public int DEFAULT_PLAYER_HP = 100;
	static public int DEFAULT_CUBE_HP = 30;

	static public double FIGHT_MIN_HP = 0.5d;
	static public double FIGHT_CALC_LOW = 0.25d;

	static public long GAME_TIMEOUT = 60000L * 60L;
	static public int EXPECTED_CUBE_COUNT = 2;
	static public int DECREASE_BOARD_BY_TURN = 20;
	static public int DECREASE_HP_BY_OUT_BORDER = 20;
	static public int DECREASE_HP_BY_STEP = 3;
	
	static public int ERROR_CODE_MISSING_DATA = 1;
	static public int ERROR_CODE_GAME_CLOSE_FAILED = 2;
	static public int ERROR_CODE_NO_GAME_BY_GAMEID = 3;
	static public int ERROR_CODE_GAME_IS_FULL = 4;
	static public int ERROR_CODE_PLAYERID_OR_SECRET_IS_WRONG = 5;
	static public int ERROR_CODE_GAME_NOT_FOUND = 6;
	static public int ERROR_CODE_INVALID_STEP = 7;
	static public int ERROR_CODE_NOT_YOUR_TURN = 8;

	static public String ERROR_MESSAGE_NO_DISPLAY_NAME = "Display name is missing";
	static public String ERROR_MESSAGE_GAME_CLOSE_FAILED = "The gameId or adminId is wrong. No game has been closed.";
	static public String ERROR_MESSAGEE_NO_GAME_BY_GAMEID = "There is no registered game with the given gameId";
	static public String ERROR_MESSAGE_GAME_IS_FULL = "The game is full.";
	static public String ERROR_MESSAGE_PLAYERID_OR_SECRET_IS_WRONG = "playerId or playerSecret is wrong";
	static public String ERROR_MESSAGE_GAME_NOT_FOUND = "Game not found.";
	static public String ERROR_MESSAGE_INVALID_STEP = "Invalid step. One direction must be zero (dX or dY)";
	static public String ERROR_MESSAGE_NOT_YOUR_TURN = "It's not your turn.";

}
