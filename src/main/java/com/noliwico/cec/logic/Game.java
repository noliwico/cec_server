package com.noliwico.cec.logic;

import java.util.ArrayList;
import java.util.Iterator;

public class Game {
	private String name;
	private String id;
	private String adminId;
	private int boardWidth = 8;
	private int boardHeight = 8;
	private int maxPlayers = 4;
	private int gameStatus = GameConstants.STATUS_CREATED;
	private ArrayList<Cube> cubes;
	private ArrayList<Player> players;
	private long created;
	private int cpi = 0; //current player index
	private int turnCounter = 0;
	
	public Game(Game game) {
		this.name = game.getName();
		this.id = game.getId();
		this.adminId = game.getAdminId();
		this.boardHeight = game.getBoardHeight();
		this.boardWidth = game.getBoardWidth();
		this.maxPlayers = game.getMaxPlayers();
		this.gameStatus = game.getGameStatus();
		this.created = game.getCreated();
		this.cpi = game.getCpi();
		this.turnCounter = game.getTurnCounter();
		this.cubes = new ArrayList<Cube>();
		for (Iterator<Cube> iterator = game.getCubes().iterator(); iterator.hasNext();) {
			this.cubes.add(new Cube(iterator.next()));
		}
		this.players = new ArrayList<Player>();
		for (Iterator<Player> iterator = game.getPlayers().iterator(); iterator.hasNext();) {
			this.players.add(new Player(iterator.next()));
		}
	}
	
	public Game() {
		this.created = System.currentTimeMillis();
		this.cubes = new ArrayList<Cube>();
		this.players = new ArrayList<Player>();
	}
	
	public String getNextPlayerId() {
		if ( players.size() > 0 ) {
			return players.get(getCpi()).getId();
		}
		return "";
	}
	
	public Player getPlayerById( String playerId, String playerSecret) {
		Player retVal = null;
		for ( Iterator<Player> playersIterator = this.players.iterator(); playersIterator.hasNext();) {
			Player player = playersIterator.next();
			if ( player.getId().equals(playerId) && player.getSecret().equals(playerSecret) ) {
				retVal = player;
				break;
			}
		}
		return retVal;
	}
	public int getTurnCounter() {
		return turnCounter;
	}

	public void setTurnCounter(int turnCounter) {
		this.turnCounter = turnCounter;
	}

	public int getCpi() {
		return cpi;
	}
	public void setCpi(int cpi) {
		this.cpi = cpi;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getBoardWidth() {
		return boardWidth;
	}
	public void setBoardWidth(int boardWidth) {
		this.boardWidth = boardWidth;
	}
	public int getBoardHeight() {
		return boardHeight;
	}
	public void setBoardHeight(int boardHeight) {
		this.boardHeight = boardHeight;
	}
	public int getMaxPlayers() {
		return maxPlayers;
	}
	public void setMaxPlayers(int maxPlayers) {
		this.maxPlayers = maxPlayers;
	}
	public int getGameStatus() {
		return gameStatus;
	}
	public void setGameStatus(int gameStatus) {
		this.gameStatus = gameStatus;
	}
	public ArrayList<Cube> getCubes() {
		return cubes;
	}
	public void setCubes(ArrayList<Cube> cubes) {
		this.cubes = cubes;
	}
	public ArrayList<Player> getPlayers() {
		return players;
	}
	public void setPlayers(ArrayList<Player> players) {
		this.players = players;
	}
	public long getCreated() {
		return created;
	}
	public void setCreated(long created) {
		this.created = created;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAdminId() {
		return adminId;
	}

	public void setAdminId(String adminId) {
		this.adminId = adminId;
	}
	
	
	
	

}
