package com.noliwico.cec.logic;

public class Cube {
	private String id;
	private int posX;
	private int posY;
	private int hp;
	private boolean npc;
	
	public Cube(Cube cube) {
		this.id = cube.getId();
		this.posX = cube.getPosX();
		this.posY = cube.getPosY();
		this.hp = cube.getHp();
		this.npc = cube.isNpc();
	}
	
	public Cube() {
		
	}
	
	public int getPosX() {
		return posX;
	}
	public void setPosX(int posX) {
		this.posX = posX;
	}
	public int getPosY() {
		return posY;
	}
	public void setPosY(int posY) {
		this.posY = posY;
	}
	public boolean isNpc() {
		return npc;
	}
	public void setNpc(boolean npc) {
		this.npc = npc;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getHp() {
		return hp;
	}

	public void setHp(int hp) {
		this.hp = hp;
	}
	

}
