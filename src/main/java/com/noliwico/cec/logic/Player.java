package com.noliwico.cec.logic;

public class Player extends Cube {
	private String name;
	private String secret;
	
	public Player() {
		
	}
	
	public Player(Player player) {
		super(player);
		this.name = player.getName();
		this.secret = player.getSecret();
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSecret() {
		return secret;
	}
	public void setSecret(String secret) {
		this.secret = secret;
	}

	
	

}
