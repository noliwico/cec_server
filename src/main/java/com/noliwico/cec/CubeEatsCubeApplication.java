package com.noliwico.cec;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class CubeEatsCubeApplication {

	public static void main(String[] args) {
		SpringApplication.run(CubeEatsCubeApplication.class, args);
	}

}
