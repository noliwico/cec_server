package com.noliwico.cec.controllers;

import java.util.ArrayList;
import java.util.Iterator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.noliwico.cec.api.CloseGameReply;
import com.noliwico.cec.api.CloseGameRequest;
import com.noliwico.cec.api.CreateNewGameReply;
import com.noliwico.cec.api.CreateNewGameRequest;
import com.noliwico.cec.api.CubeDetails;
import com.noliwico.cec.api.DoStepReply;
import com.noliwico.cec.api.DoStepRequest;
import com.noliwico.cec.api.GameStatus;
import com.noliwico.cec.api.GetGamesReply;
import com.noliwico.cec.api.GetGamesRequest;
import com.noliwico.cec.api.GetHistoryReply;
import com.noliwico.cec.api.GetHistoryRequest;
import com.noliwico.cec.api.GetStatusReply;
import com.noliwico.cec.api.GetStatusRequest;
import com.noliwico.cec.api.SignUpReply;
import com.noliwico.cec.api.SignUpRequest;
import com.noliwico.cec.logic.Cube;
import com.noliwico.cec.logic.ErrorHolder;
import com.noliwico.cec.logic.Game;
import com.noliwico.cec.logic.GameConstants;
import com.noliwico.cec.logic.GameLogic;
import com.noliwico.cec.logic.Player;

@CrossOrigin(origins = "http://localhost:4200", maxAge=36000)
@RestController
public class GameController {

	@Autowired
	GameLogic gameLogic;

	@PostMapping("/createNewGame")
	CreateNewGameReply createNewGame(@RequestBody CreateNewGameRequest request) {
		CreateNewGameReply reply = new CreateNewGameReply();
		if (request.getDisplayName() == null || "".equals(request.getDisplayName())) {
			reply.setErrorCode(GameConstants.ERROR_CODE_MISSING_DATA);
			reply.setErrorMessage(GameConstants.ERROR_MESSAGE_NO_DISPLAY_NAME);
			return reply;
		}
		Game game = gameLogic.createNewGame(request.getDisplayName(), request.getMaxPlayers());
		reply.setAdminId(game.getAdminId());
		reply.setDisplayName(game.getName());
		reply.setGameId(game.getId());
		reply.setMaxPlayers(game.getMaxPlayers());
		return reply;

	}

	@PostMapping("/closeGame")
	CloseGameReply closeGame(@RequestBody CloseGameRequest request) {
		boolean success = gameLogic.closeGame(request.getGameId(), request.getAdminId());

		CloseGameReply reply = new CloseGameReply(request.getGameId(), success);
		if (!success) {
			reply.setErrorCode(GameConstants.ERROR_CODE_GAME_CLOSE_FAILED);
			reply.setErrorMessage(GameConstants.ERROR_MESSAGE_GAME_CLOSE_FAILED);
		}
		return reply;
	}

	@PostMapping("/getGames")
	GetGamesReply getGames(@RequestBody GetGamesRequest request) {
		GetGamesReply reply = new GetGamesReply();
		reply.setGames(new ArrayList<GameStatus>());
		for (Iterator<Game> games = gameLogic.getGames().iterator(); games.hasNext();) {
			Game game = games.next();
			GameStatus replyGameStatus = new GameStatus();
			fillGameStatus(replyGameStatus, game, null);
			reply.getGames().add(replyGameStatus);
		}
		return reply;
	}

	@PostMapping("/signUp")
	SignUpReply signUp(@RequestBody SignUpRequest request) {
		SignUpReply reply = new SignUpReply();
		ErrorHolder errorHolder = new ErrorHolder();
		Player player = gameLogic.signUp(request.getGameId(), request.getPlayerName(), errorHolder);
		if (player == null) {
			reply.setErrorCode(errorHolder.getErrorCode());
			reply.setErrorMessage(errorHolder.getErrorMessage());
			return reply;
		}
		Game game = gameLogic.getGameById(request.getGameId());
		reply.setBoardHeight(game.getBoardHeight());
		reply.setBoardWidth(game.getBoardWidth());
		reply.setGameId(game.getId());
		reply.setPlayerId(player.getId());
		reply.setPlayerSecret(player.getSecret());

		return reply;
	}
	
	private void fillGameStatus(GameStatus replyGameStatus, Game game, String nextPlayerId) {
		replyGameStatus.setGameName(game.getName());
		replyGameStatus.setCubes(new ArrayList<CubeDetails>());
		replyGameStatus.setGameId(game.getId());
		replyGameStatus.setStatus(game.getGameStatus());
		replyGameStatus.setNextPlayerId(game.getGameStatus()==GameConstants.STATUS_RUNNING?nextPlayerId:null);
		replyGameStatus.setMaxPlayers(game.getMaxPlayers());
		replyGameStatus.setBoardHeight(game.getBoardHeight());
		replyGameStatus.setBoardWidth(game.getBoardWidth());
		for (Iterator<Player> playerIterator = game.getPlayers().iterator(); playerIterator.hasNext();) {
			Player currentPlayer = playerIterator.next();
			CubeDetails cd = new CubeDetails();
			cd.setHealthPoint(currentPlayer.getHp());
			cd.setPlayerId(currentPlayer.getId());
			cd.setPlayerName(currentPlayer.getName());
			cd.setPosX(currentPlayer.getPosX());
			cd.setPosY(currentPlayer.getPosY());
			cd.setNpc(currentPlayer.isNpc());
			replyGameStatus.getCubes().add(cd);
		}
		for (Iterator<Cube> cubeIterator = game.getCubes().iterator(); cubeIterator.hasNext();) {
			Cube currentCube = cubeIterator.next();
			CubeDetails cd = new CubeDetails();
			cd.setHealthPoint(currentCube.getHp());
			cd.setPosX(currentCube.getPosX());
			cd.setPosY(currentCube.getPosY());
			cd.setNpc(currentCube.isNpc());
			replyGameStatus.getCubes().add(cd);
		}		
	}

	@PostMapping("/getStatus")
	GetStatusReply getStatus(@RequestBody GetStatusRequest request) {
		GetStatusReply reply = new GetStatusReply();
		GameStatus replyGameStatus = new GameStatus();
		Game game = gameLogic.getGameById(request.getGameId());
		if (game != null) {
			reply.setGameStatus(replyGameStatus);
			Player player = game.getPlayerById(request.getPlayerId(), request.getPlayerSecret());
			String nextPlayerId = player == null ? null : game.getNextPlayerId();
			fillGameStatus(replyGameStatus, game, nextPlayerId);
			if (player == null) {
				reply.setErrorCode(GameConstants.ERROR_CODE_PLAYERID_OR_SECRET_IS_WRONG);
				reply.setErrorMessage(GameConstants.ERROR_MESSAGE_PLAYERID_OR_SECRET_IS_WRONG);
			}
		} else {
			reply.setErrorCode(GameConstants.ERROR_CODE_GAME_NOT_FOUND);
			reply.setErrorMessage(GameConstants.ERROR_MESSAGE_GAME_NOT_FOUND);
		}

		return reply;
	}

	@PostMapping("/doStep")
	DoStepReply doStep(@RequestBody DoStepRequest request) {
		DoStepReply reply = new DoStepReply(false);
		Game game = gameLogic.getGameById(request.getGameId());
		if (game != null) {
			Player player = game.getPlayerById(request.getPlayerId(), request.getPlayerSecret());
			if (player != null) {
				if (player.getId() == game.getNextPlayerId()) {
					if (!gameLogic.doStep(game, player, request.getStep().getdX(), request.getStep().getdY())) {
						reply.setErrorCode(GameConstants.ERROR_CODE_INVALID_STEP);
						reply.setErrorMessage(GameConstants.ERROR_MESSAGE_INVALID_STEP);
					} else {
						reply.setStepSuccess(true);
						gameLogic.nextPlayer(game);
					}
				} else {
					reply.setErrorCode(GameConstants.ERROR_CODE_NOT_YOUR_TURN);
					reply.setErrorMessage(GameConstants.ERROR_MESSAGE_NOT_YOUR_TURN);
				}
			} else {
				reply.setErrorCode(GameConstants.ERROR_CODE_PLAYERID_OR_SECRET_IS_WRONG);
				reply.setErrorMessage(GameConstants.ERROR_MESSAGE_PLAYERID_OR_SECRET_IS_WRONG);
			}
		} else {
			reply.setErrorCode(GameConstants.ERROR_CODE_GAME_NOT_FOUND);
			reply.setErrorMessage(GameConstants.ERROR_MESSAGE_GAME_NOT_FOUND);
		}

		return reply;

	}
	
	@PostMapping("/getHistory")
	GetHistoryReply getHistory(@RequestBody GetHistoryRequest request) {
		GetHistoryReply reply = new GetHistoryReply();
		reply.setHistoryItems(new ArrayList<GameStatus>());
		Game game = gameLogic.getGameById(request.getGameId());
		if (game != null) {
			ArrayList<Game> history = gameLogic.getGameHistory().get(game.getId());

			if ( history != null ) {
				for ( int i = request.getFrom(); i < history.size(); i++) {
					Game historyItem = history.get(i);
					GameStatus currentGameStatus = new GameStatus();
					fillGameStatus(currentGameStatus, historyItem, null);
					reply.getHistoryItems().add(currentGameStatus);
				}
			}
		} else {
			reply.setErrorCode(GameConstants.ERROR_CODE_GAME_NOT_FOUND);
			reply.setErrorMessage(GameConstants.ERROR_MESSAGE_GAME_NOT_FOUND);
		}

		return reply;
	}

}
